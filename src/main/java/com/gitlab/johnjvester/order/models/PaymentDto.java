package com.gitlab.johnjvester.order.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/*
This object would really be in a shared Java library.
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class PaymentDto {
    private int id;
    private String transactionId;
    private BigDecimal amount;
    private int customerId;
}
