package com.gitlab.johnjvester.order.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class OrderRequest {
    private String description;
    private String emailAddress;
    private String name;
    private BigDecimal amount;
}
