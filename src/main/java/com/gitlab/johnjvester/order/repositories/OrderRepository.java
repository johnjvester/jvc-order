package com.gitlab.johnjvester.order.repositories;

import com.gitlab.johnjvester.order.entities.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Integer> { }
