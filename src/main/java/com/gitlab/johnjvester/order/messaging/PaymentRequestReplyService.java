package com.gitlab.johnjvester.order.messaging;

import com.gitlab.johnjvester.order.configs.MessagingConfigurationProperties;
import com.gitlab.johnjvester.order.models.PaymentDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Slf4j
@RequiredArgsConstructor
@Component
public class PaymentRequestReplyService {
    private final DirectExchange paymentDirectExchange;
    private final MessagingConfigurationProperties messagingConfigurationProperties;
    private final RabbitTemplate rabbitTemplate;

    public PaymentDto getPayment(int customerId, BigDecimal amount) {
        PaymentDto paymentDto = new PaymentDto();
        paymentDto.setCustomerId(customerId);
        paymentDto.setAmount(amount);

        return rabbitTemplate.convertSendAndReceiveAsType(paymentDirectExchange.getName(),
                messagingConfigurationProperties.getPaymentRoutingKey(),
                paymentDto,
                new ParameterizedTypeReference<>() {});
    }
}
