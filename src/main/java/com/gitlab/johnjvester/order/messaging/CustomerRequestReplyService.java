package com.gitlab.johnjvester.order.messaging;

import com.gitlab.johnjvester.order.configs.MessagingConfigurationProperties;
import com.gitlab.johnjvester.order.models.CustomerDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class CustomerRequestReplyService {
    private final DirectExchange customerDirectExchange;
    private final MessagingConfigurationProperties messagingConfigurationProperties;
    private final RabbitTemplate rabbitTemplate;

    public CustomerDto getCustomer(String emailAddress, String name) {
        CustomerDto customerDto = new CustomerDto();
        customerDto.setEmailAddress(emailAddress);
        customerDto.setName(name);

        return rabbitTemplate.convertSendAndReceiveAsType(customerDirectExchange.getName(),
                messagingConfigurationProperties.getCustomerRoutingKey(),
                customerDto,
                new ParameterizedTypeReference<>() {});
    }
}
