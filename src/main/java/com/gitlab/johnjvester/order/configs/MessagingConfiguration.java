package com.gitlab.johnjvester.order.configs;

import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@RequiredArgsConstructor
@Configuration
@EnableRabbit
public class MessagingConfiguration {
    private final MessagingConfigurationProperties messagingConfigurationProperties;

    @Bean(name = "customerDirectExchange")
    public DirectExchange customerDirectExchange() {
        return new DirectExchange(messagingConfigurationProperties.getCustomerDirectExchange());
    }

    @Bean(name = "paymentDirectExchange")
    public DirectExchange paymentDirectExchange() {
        return new DirectExchange(messagingConfigurationProperties.getPaymentDirectExchange());
    }

    @Bean
    public MessageConverter jackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }
}
