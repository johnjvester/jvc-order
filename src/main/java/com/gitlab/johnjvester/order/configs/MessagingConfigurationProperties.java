package com.gitlab.johnjvester.order.configs;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.validation.constraints.NotNull;

@Data
@Configuration("messagingConfigurationProperties")
@ConfigurationProperties(prefix = "messaging")
public class MessagingConfigurationProperties {
    @NotNull
    private String customerDirectExchange;

    @NotNull
    private String customerRoutingKey;

    @NotNull
    private String paymentDirectExchange;

    @NotNull
    private String paymentRoutingKey;
}
