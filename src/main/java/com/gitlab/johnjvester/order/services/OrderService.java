package com.gitlab.johnjvester.order.services;

import com.gitlab.johnjvester.order.entities.Order;
import com.gitlab.johnjvester.order.messaging.CustomerRequestReplyService;
import com.gitlab.johnjvester.order.messaging.PaymentRequestReplyService;
import com.gitlab.johnjvester.order.models.CustomerDto;
import com.gitlab.johnjvester.order.models.OrderRequest;
import com.gitlab.johnjvester.order.models.PaymentDto;
import com.gitlab.johnjvester.order.repositories.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.NoSuchElementException;

@Slf4j
@RequiredArgsConstructor
@Service
public class OrderService {
    private final CustomerRequestReplyService customerRequestReplyService;
    private final OrderRepository orderRepository;
    private final PaymentRequestReplyService paymentRequestReplyService;

    public List<Order> getOrders() {
        return orderRepository.findAll();
    }

    public Order getOrder(int id) {
        return orderRepository.findById(id).orElseThrow(NoSuchElementException::new);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Order processOrder(OrderRequest orderRequest) {
        log.debug("processOrder(orderRequest={})", orderRequest);

        CustomerDto customerDto = customerRequestReplyService.getCustomer(orderRequest.getEmailAddress(), orderRequest.getName());
        log.debug("customerDto={}", customerDto);

        PaymentDto paymentDto = paymentRequestReplyService.getPayment(customerDto.getId(), orderRequest.getAmount());
        log.debug("paymentDto={}", paymentDto);

        Order order = new Order();
        order.setOrderDate(LocalDateTime.now());
        order.setDescription(orderRequest.getDescription());
        order.setCustomerId(customerDto.getId());
        order.setPaymentId(paymentDto.getId());
        orderRepository.save(order);

        log.info("order={}", order);
        return order;
    }
}
