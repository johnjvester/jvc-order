CREATE TABLE orders (
                        id INT PRIMARY KEY NOT NULL,
                        customer_id INT NOT NULL,
                        payment_id INT NOT NULL,
                        order_date timestamp NOT NULL,
                        description VARCHAR(255)
);